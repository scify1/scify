# SciFy
_I need to get some sleep_

[![forthebadge](https://forthebadge.com/images/badges/made-with-crayons.svg)](https://forthebadge.com)

SciFy was made when I (Chickenmanfy) thought it would be fun to learn how to make mods in Quilt. It has since been moved to Fabric though, since the community wanted that.

I then remembered when one of my friends (BearCat865) suggested making a Dungeonfy mod, and here we are. ("Here" being with a blank slate because currently it's not done.)

---
# Roadmap
_Currently in super beta so don't expect something working for a while._
- Right shift menu to enable and disable mods (12%)
- Health & Food bars replaced with dynamic bar similar to EXP bar (0%)
- Custom GUI for /warp menu and stats viewers (0%)
- Built-in guide (0%)
- Auto-Welcome (0%) **WARNING: This feature may prove to be annoying, in which case we will remove it.**
